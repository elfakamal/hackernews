import fetch from 'isomorphic-fetch';

import { API_URL, CATEGORIES } from '../constants';

export const getStoryIds = async (endpoint = CATEGORIES[0].endpoint) => {
  const response = await fetch(`${API_URL}/${endpoint}.json`);
  return await response.json();
}

export const fetchStories = async storyIds => await Promise.all(
  storyIds.map(
    async id => await (await fetch(`${API_URL}/item/${id}.json`)).json(),
  )
);
