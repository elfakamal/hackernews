import React from 'react';
import PropTypes from 'prop-types';

import { CATEGORIES } from '../constants';

const Filters = ({ onFilterClick, disabled }) => (
  <div className="hackernews-app-body-filters">
    {CATEGORIES.map(category => (
      <button
        disabled={disabled}
        key={category.name}
        onClick={() => onFilterClick(category.endpoint)}
      >
        {category.name}
      </button>
    ))}
  </div>
);

Filters.propTypes = {
  onFilterClick: PropTypes.func,
  disabled: PropTypes.bool,
};

export default Filters;
